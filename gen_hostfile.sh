#!/bin/bash

ppn=$1
red="\033[01;31m"
green="\033[01;32m"

if [[ $ppn == "" ]]
then
  echo -e $red"Correct usage: ./gen_hostsfile.sh <#processes on each node>"
  echo -e $red"Exiting..."
  exit 0
fi

if [ ! -z $SLURM_JOBID ]
then
    JOBID=$SLURM_JOBID
    hostfile=hosts
    rm -f $hostfile
    for i in `scontrol show hostnames $SLURM_NODELIST`
    do
      for (( j=0; j<$ppn; j++ ))
      do
        echo $i>>$hostfile
      done
    done
elif [ ! -z $PBS_JOBID ]
then
    JOBID=$PBS_JOBID
    hostfile=hosts
    rm -f $hostfile
    for i in `uniq $PBS_NODEFILE`
    do
      for (( j=0; j<$ppn; j++ ))
      do
        echo $i>>$hostfile
      done
    done
elif [ ! -z $LSB_JOBID ]
then
    JOBID=$LSB_JOBID
    hostfile=hosts
    rm -f $hostfile
    k=0;
    for i in `uniq $LSB_DJOB_HOSTFILE`
    do
      k=`echo "$k+1" | bc`
      if [ "$k" == "1" ]
      then
        #Skip launch node
        continue;
      fi
      for (( j=0; j<$ppn; j++ ))
      do
        echo $i>>$hostfile
      done
    done
else
    echo "Unknown job scheduler"
    exit
fi

h="`wc -l $hostfile`"
r="`cut -f1 -d "-" hosts | uniq`"

if [ -s $hostfile ]
then
  #echo "$hostfile file created with $h. Racks are $r"
  echo "$hostfile file created with $h."
fi

echo ""
